﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Database.Entities
{
    public partial class SerialNumber
    {
        public SerialNumber()
        {
            LicenseLogs = new HashSet<LicenseLog>();
        }

        public int Id { get; set; }
        public string Number { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreateDate { get; set; }
        public string SiteName { get; set; }

        public virtual License License { get; set; }
        public virtual ICollection<LicenseLog> LicenseLogs { get; set; }
    }
}
