﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Database.Entities
{
    public partial class License
    {
        public int Id { get; set; }
        public int SerialNumberId { get; set; }
        public string UniqueId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreateDate { get; set; }

        public virtual SerialNumber SerialNumber { get; set; }
    }
}
