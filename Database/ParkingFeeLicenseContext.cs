﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database.Entities
{
    public partial class ParkingFeeLicenseContext : DbContext
    {
        public ParkingFeeLicenseContext()
        {
        }

        public ParkingFeeLicenseContext(DbContextOptions<ParkingFeeLicenseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<License> Licenses { get; set; }
        public virtual DbSet<SerialNumber> SerialNumbers { get; set; }
        public virtual DbSet<LicenseLog> LicenseLogs { get; set; }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer("Server=localhost ;Database=parkingfeelicense; Uid=dev; pwd=dev1234;");
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);

            modelBuilder.Entity<License>(entity =>
            {
                entity.ToTable("license");

                entity.HasIndex(e => e.Id).HasName("IDX_license_id");

                entity.HasIndex(e => e.SerialNumberId).HasName("IDX_license_serialNumberId");

                entity.HasIndex(e => e.UniqueId).HasName("IDX_license_uniqueId");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SerialNumberId)
                    .HasColumnName("serialNumberId")              
                    .IsRequired(true);

                entity.Property(e => e.UniqueId)
                    .HasColumnName("uniqueId")
                    .HasMaxLength(255)
                    .IsRequired(true);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("getDate()")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.SerialNumber)
                    .WithOne(p => p.License)
                    .HasForeignKey<License>(d => d.SerialNumberId)
                    .HasConstraintName("FK_license_serialNumberId_serial_number_id");
            });

            modelBuilder.Entity<LicenseLog>(entity =>
            {
                entity.ToTable("license_log");

                entity.HasIndex(e => e.Id).HasName("IDX_license_log_id");

                entity.HasIndex(e => e.SerialNumberId).HasName("IDX_license_log_serialNumberId");

                entity.HasIndex(e => e.UniqueId).HasName("IDX_license_log_uniqueId");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SerialNumberId)
                    .HasColumnName("serialNumberId")
                    .IsRequired(true);

                entity.Property(e => e.UniqueId)
                    .HasColumnName("uniqueId")
                    .HasMaxLength(255)
                    .IsRequired(true);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("getDate()")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ActionBy)
                    .HasColumnName("actionBy")
                    .HasMaxLength(255)
                    .IsRequired(true);

                entity.HasOne(d => d.SerialNumber)
                    .WithMany(p => p.LicenseLogs)
                    .HasForeignKey(d => d.SerialNumberId)
                    .HasConstraintName("FK_license_log_serialNumberId_serial_number_id");
            });

            modelBuilder.Entity<SerialNumber>(entity =>
            {
                entity.ToTable("serial_number");

                entity.HasIndex(e => e.Id).HasName("IDX_serial_number_id");

                entity.HasIndex(e => e.Number).HasName("IDX_serial_number_number");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Number)
                    .HasColumnName("number")
                    .HasMaxLength(255)
                    .IsRequired(true);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("getDate()")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SiteName)
                    .HasColumnName("siteName")
                    .HasMaxLength(255)
                    .IsRequired(true);                
            });
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
