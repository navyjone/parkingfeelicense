﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class initial_with_serial_number_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "serial_number",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    number = table.Column<string>(maxLength: 255, nullable: false),
                    createDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getDate()"),
                    siteName = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_serial_number", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IDX_serial_number_id",
                table: "serial_number",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IDX_serial_number_number",
                table: "serial_number",
                column: "number");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "serial_number");
        }
    }
}
