﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_table_license_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "license_log",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    createDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getDate()"),
                    serialNumberId = table.Column<int>(nullable: false),
                    uniqueId = table.Column<string>(maxLength: 255, nullable: false),
                    actionBy = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_license_log", x => x.id);
                    table.ForeignKey(
                        name: "FK_license_log_serialNumberId_serial_number_id",
                        column: x => x.serialNumberId,
                        principalTable: "serial_number",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IDX_license_log_id",
                table: "license_log",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IDX_license_log_serialNumberId",
                table: "license_log",
                column: "serialNumberId");

            migrationBuilder.CreateIndex(
                name: "IDX_license_log_uniqueId",
                table: "license_log",
                column: "uniqueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "license_log");
        }
    }
}
