﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_table_license : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "license",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    serialNumberId = table.Column<int>(nullable: false),
                    uniqueId = table.Column<string>(maxLength: 255, nullable: false),
                    createDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getDate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_license", x => x.id);
                    table.ForeignKey(
                        name: "FK_license_serialNumberId_serial_number_id",
                        column: x => x.serialNumberId,
                        principalTable: "serial_number",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IDX_license_id",
                table: "license",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IDX_license_serialNumberId",
                table: "license",
                column: "serialNumberId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IDX_license_uniqueId",
                table: "license",
                column: "uniqueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "license");
        }
    }
}
