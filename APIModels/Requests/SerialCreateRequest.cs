﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class SerialCreateRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string SiteName { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string SiteCode { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int Total { get; set; }
    }
}
