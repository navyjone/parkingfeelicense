﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class SerialActivateRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string SerialNumber { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string UniqueId { get; set; }
    }
}
