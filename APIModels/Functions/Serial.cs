﻿using APIModels.Requests;
using APIModels.Responses;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace APIModels.Functions
{

    public interface ISerial
    {
        Task<HttpResponse<string>> Create(SerialCreateRequest request);
        Task<HttpResponse<string>> Activate(string uniqueId, string serialNumber);
        Task<HttpResponse<string>> UnActivate(string uniqueId, string serialNumber, string role);
        Task<HttpResponse<string>> Validate(string uniqueId, string serialNumber);
    }

    public class Serial : ISerial
    {
        private readonly ParkingFeeLicenseContext context;
        private readonly IConfiguration appConfig;

        public Serial (ParkingFeeLicenseContext context, IConfiguration appConfig)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        public async Task<HttpResponse<string>> Create(SerialCreateRequest request)
        {
            var serials = new List<SerialNumber>();
            var prefix = $"{request.SiteCode}";
            var codes = Code.Generate(prefix, request.Total, int.Parse(appConfig["SerialCodeRange"]) - prefix.Length);
            if (codes.Length == 0) return new HttpResponse<string> { StatusCode = HttpStatusCode.BadRequest, Object = "Abnormal code range" };
            for (var i = 0; i < request.Total; i++)
            {
                serials.Add(new SerialNumber
                {
                    SiteName = request.SiteName,
                    Number = codes[i]
                });
            }

            await context.AddRangeAsync(serials);
            await context.SaveChangesAsync();
            return new HttpResponse<string> { StatusCode = HttpStatusCode.Created, Object = "Serials are created." };
        }

        public async Task<HttpResponse<string>> Activate(string uniqueId, string serialNumber)
        {
            var serial = await context.SerialNumbers.Where(s => s.Number == serialNumber).AsNoTracking().FirstOrDefaultAsync();
            if (serial is null) return new HttpResponse<string> { StatusCode = HttpStatusCode.NotFound, Object = "Serial number does not exist." };

            var license = await context.Licenses.Where(l => l.SerialNumberId == serial.Id &&
                                                            l.UniqueId == uniqueId)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();
            if (license != null)
            {
                return new HttpResponse<string>
                {
                    StatusCode = HttpStatusCode.OK,
                    Object = Crypto.EncryptPassword(appConfig["PasswordSalt"], new string[] { uniqueId, serialNumber })
                };
            }

            var licenseBySerial = await context.Licenses.Where(l => l.SerialNumberId == serial.Id).AsNoTracking().FirstOrDefaultAsync();
            if (licenseBySerial != null) return new HttpResponse<string> { StatusCode = HttpStatusCode.Conflict, Object = "Serial number is already activated." };

            var licenseByUniqueId = await context.Licenses.Where(l => l.UniqueId == uniqueId).AsNoTracking().FirstOrDefaultAsync();
            if (licenseByUniqueId != null) return new HttpResponse<string> { StatusCode = HttpStatusCode.Conflict, Object = "This device is already activated." };

            await context.AddAsync(new License
            {
                SerialNumberId = serial.Id,
                UniqueId = uniqueId
            });

            await context.AddAsync(new LicenseLog
            {
                SerialNumberId = serial.Id,
                UniqueId = uniqueId,
                ActionBy = uniqueId
            });

            await context.SaveChangesAsync();

            return new HttpResponse<string>
            {
                StatusCode = HttpStatusCode.OK,
                Object = Crypto.EncryptPassword(appConfig["PasswordSalt"], new string[] { uniqueId, serialNumber })
            };
        }

        public async Task<HttpResponse<string>> UnActivate(string uniqueId, string serialNumber, string role)
        {
            var validateResult = await this.Validate(uniqueId, serialNumber);
            if (validateResult != null) return validateResult;

            var serial = await context.SerialNumbers.Where(s => s.Number == serialNumber).AsNoTracking().FirstOrDefaultAsync();
            var license = await context.Licenses.Where(l => l.SerialNumberId == serial.Id && l.UniqueId == uniqueId)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync();
            context.Remove(license);

            await context.AddAsync(new LicenseLog
            {
                SerialNumberId = serial.Id,
                UniqueId = "",
                ActionBy = string.IsNullOrEmpty(role) ? uniqueId : role
            });

            await context.SaveChangesAsync();

            return new HttpResponse<string> { StatusCode = HttpStatusCode.OK };
        }

        public async Task<HttpResponse<string>> Validate(string uniqueId, string serialNumber)
        {
            var serial = await context.SerialNumbers.Where(s => s.Number == serialNumber).AsNoTracking().FirstOrDefaultAsync();
            if (serial is null) return new HttpResponse<string> { StatusCode = HttpStatusCode.NotFound, Object = "Serial number does not exist." };

            var license = await context.Licenses.Where(l => l.SerialNumberId == serial.Id && l.UniqueId == uniqueId)
                                                        .AsNoTracking()
                                                        .FirstOrDefaultAsync();
            if (license is null) return new HttpResponse<string> { StatusCode = HttpStatusCode.NotFound, Object = "This activation is not found." };

            return null; 
        }
    }
}
