﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace APIModels.Responses
{
    public class HttpResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public T Object { get; set; }
    }
}
