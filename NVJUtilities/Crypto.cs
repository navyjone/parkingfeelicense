﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace NVJUtilities
{
    public class Crypto
    {
        public static string EncodeBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string DecodeBase64(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string EncodeSha256(string input)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(input), 0, Encoding.UTF8.GetByteCount(input));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }

            return hash.ToString();
        }

        public static string EncryptPassword(IConfiguration appConfig, string username, string password)
        {
            return EncodeSha256($"{username}{appConfig["passwordSalt"]}{password}");
        }

        public static string EncryptPassword(string password, IEnumerable<string> texts)
        {
            var key = $"{string.Join("", texts)}{password}";
            var swappedKey = Swap(key);
            return EncodeSha256($"{swappedKey}");
        }

        public static string Swap(string key)
        {
            var position = (int)Math.Ceiling((decimal)key.Length / 2);
            var firstPart = key.Substring(0, position);
            var secondPart = key.Substring(position);

            var swapKey = "";
            for(var i = 0; i < firstPart.Length; i++)
            {
                if (i < secondPart.Length) swapKey += secondPart[i];
                if (i < firstPart.Length) swapKey += firstPart[i];
            }

            return swapKey;
        }
    }
}
