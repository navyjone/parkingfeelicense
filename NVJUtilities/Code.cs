﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NVJUtilities
{
    public class Code
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
       
        public static string[] Generate(string prefix, int total, int codeRange = 2)
        {
            // code range need equal or more than 2
            if (total > Math.Pow(chars.Length, codeRange) || codeRange > chars.Length) return new string[0];
            var codeGens = new Dictionary<string, string>();
            var random = new Random();

            while (codeGens.Count < total)
            {
                var code = $"{prefix}";
                for(var i = 0; i < codeRange; i++)
                {
                    var randomPos = random.Next(0, chars.Count());
                    code += chars[randomPos];
                }

                if (!codeGens.ContainsKey(code)) codeGens.Add(code, code);
            }

            return codeGens.Values.ToArray();
        }
    }
}
