﻿using APIModels.Functions;
using APIModels.Requests;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TestUtilities;
using TestUtilities.Generators;
using Xunit;

namespace Test
{
    public class SerialTest : IDisposable
    {
        private ParkingFeeLicenseContext context;
        private IConfiguration config;
        private ISerial Serial;

        public SerialTest()
        {
            this.context = TestUtils.GetMemoryContext($"Serial{Guid.NewGuid().ToString()}");
            this.config = AppConfig.InitConfiguration();
            this.Serial = new Serial(context, config);
        }

        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task create()
        {
            // arrange
            var serialRequest = new SerialCreateRequest
            {
                SiteName = "A",
                SiteCode = "CODE",
                Total = 2
            };

            // act
            var result = await Serial.Create(serialRequest);
            var serials = await context.SerialNumbers.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Equal(serialRequest.Total, serials.Count);
            Assert.Equal(serialRequest.SiteName, serials[0].SiteName);
            Assert.StartsWith(serialRequest.SiteCode, serials[0].Number);
            Assert.Equal(serialRequest.SiteName, serials[1].SiteName);
            Assert.StartsWith(serialRequest.SiteCode, serials[1].Number);
        }

        [Fact]
        public async Task create_with_total_0_should_return_bad_request()
        {
            // arrange
            var serialRequest = new SerialCreateRequest
            {
                SiteName = "A",
                SiteCode = "CODE"
            };

            // act
            var result = await Serial.Create(serialRequest);
            var serials = await context.SerialNumbers.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.Empty(serials);
        }

        [Fact]
        public async Task activate()
        {
            // arrange
            var serial = await SerialGenerator.Generate(context);
            var uniqueId = "UniqueId1";

            // act
            var result = await Serial.Activate(uniqueId, serial.Number);
            var licenses = await context.Licenses.ToListAsync();
            var licenseLogs = await context.LicenseLogs.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            // TODO: Please test Object response -> to verify encrypted data response

            Assert.Single(licenses);
            Assert.Single(licenseLogs);

            Assert.Equal(serial.Id, licenses[0].SerialNumberId);
            Assert.Equal(uniqueId, licenses[0].UniqueId);

            Assert.Equal(serial.Id, licenseLogs[0].SerialNumberId);
            Assert.Equal(uniqueId, licenseLogs[0].UniqueId);
            Assert.Equal(uniqueId, licenseLogs[0].ActionBy);
        }

        [Fact]
        public async Task activate_when_there_is_no_serial_should_return_not_found()
        {
            // arrange
            var serialNumber = "No serial";
            var uniqueId = "UniqueId1";

            // act
            var result = await Serial.Activate(uniqueId, serialNumber);
            var licenses = await context.Licenses.ToListAsync();
            var licenseLogs = await context.LicenseLogs.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
            Assert.Empty(licenses);
            Assert.Empty(licenseLogs);
        }

        [Fact]
        public async Task activate_when_license_has_already_been_activated_by_yourself_should_be_ok()
        {
            // arrange
            var serial = await SerialGenerator.Generate(context);
            var license = await LicenseGenerator.Generate(context, new License
            {
                SerialNumberId = serial.Id
            });

            // act
            var result = await Serial.Activate(license.UniqueId, serial.Number);
            var licenses = await context.Licenses.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Single(licenses);
        }

        [Fact]
        public async Task activate_when_license_has_already_been_activated_by_other_should_return_conflict()
        {
            // arrange
            var serial = await SerialGenerator.Generate(context);
            var license = await LicenseGenerator.Generate(context, new License
            {
                UniqueId = "OtherUniqueId",
                SerialNumberId = serial.Id
            });

            var myUniqueId = "myUniqueId";

            // act
            var result = await Serial.Activate(myUniqueId, serial.Number);
            var licenses = await context.Licenses.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.Conflict, result.StatusCode);
            Assert.Single(licenses);
            Assert.Equal(license.UniqueId, license.UniqueId);
        }

        [Fact]
        public async Task activate_when_this_device_has_already_been_activated_with_different_serial_should_return_conflict()
        {
            // arrange
            var serial1 = await SerialGenerator.Generate(context);
            var serial2 = await SerialGenerator.Generate(context);
            var license = await LicenseGenerator.Generate(context, new License
            {
                UniqueId = "myUniqueId",
                SerialNumberId = serial1.Id
            });

            // act
            var result = await Serial.Activate(license.UniqueId, serial2.Number);
            var licenses = await context.Licenses.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.Conflict, result.StatusCode);
            Assert.Single(licenses);
            Assert.Equal(license.UniqueId, license.UniqueId);
            Assert.Equal(license.SerialNumberId, serial1.Id);
        }

        [Fact]
        public async Task validate()
        {
            // arrange
            var serial = await SerialGenerator.Generate(context);
            var license = await LicenseGenerator.Generate(context, new License
            {
                SerialNumberId = serial.Id
            });

            // act
            var result = await Serial.Validate(license.UniqueId, serial.Number);

            // assert
            Assert.Null(result);
        }

        [Fact]
        public async Task validate_when_there_is_no_serial_in_database_should_return_not_found()
        {
            // arrange
            var uniqueId = "some unique id";
            var serialToValidate = "Serial that does not exist in database";

            // act
            var result = await Serial.Validate(uniqueId, serialToValidate);

            // assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task validate_when_there_is_no_license_in_database_should_return_not_found()
        {
            // arrange
            var serial = await SerialGenerator.Generate(context);
            var uniqueId = "My Unique Id that does not activate";

            // act
            var result = await Serial.Validate(uniqueId, serial.Number);

            // assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Theory]
        [InlineData("Admin", "uniqueId1", "Admin")]
        [InlineData("", "uniqueId2", "uniqueId2")]
        [InlineData(null, "uniqueId3", "uniqueId3")]
        public async Task unactivate(string role, string uniqueId, string expectedActionBy)
        {
            // arrange
            var serial = await SerialGenerator.Generate(context);
            var license = await LicenseGenerator.Generate(context, new License
            {
                UniqueId = uniqueId,
                SerialNumberId = serial.Id
            });

            var licenseLog = await LicenseLogGenerator.Generate(context, new LicenseLog
            {
                UniqueId = uniqueId,
                SerialNumberId = serial.Id
            });

            // act
            var result = await Serial.UnActivate(license.UniqueId, serial.Number, role);
            var licenses = await context.Licenses.ToListAsync();
            var licenseLogs = await context.LicenseLogs.ToListAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Empty(licenses);
            Assert.Equal(2, licenseLogs.Count);
            Assert.Equal(expectedActionBy, licenseLogs[1].ActionBy);
            Assert.Equal(serial.Id, licenseLogs[1].SerialNumberId);
            Assert.Empty(licenseLogs[1].UniqueId);
        }
    }
}
