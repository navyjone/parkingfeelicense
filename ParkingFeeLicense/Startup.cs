using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIModels.Functions;
using Database.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace ParkingFeeLicense
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private readonly string apiVersion = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMvc();

            services.AddDbContext<ParkingFeeLicenseContext>(options => options.UseSqlServer(
                    Configuration.GetConnectionString(Configuration["SelectConnectionString"])
                )
            );

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = false,
                            RequireExpirationTime = false,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Configuration["Jwt:Issuer"],
                            ValidAudiences = new List<string>
                            {
                                Configuration["Jwt:MobileUser:Audience"],
                                Configuration["Jwt:Admin:Audience"],
                            },                         
                            IssuerSigningKeys = new List<SymmetricSecurityKey>
                            {
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:MobileUser:Key"])),
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Admin:Key"])),
                            }
                        };
                    });

            services.AddMemoryCache();

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc($"v{apiVersion}", new Microsoft.OpenApi.Models.OpenApiInfo
            //    {
            //        Title = Configuration["ProjectName"],
            //        Version = $"v{apiVersion}"
            //    });

            //    c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
            //    c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
            //        { "Bearer", Enumerable.Empty<string>() },
            //    });

            //    Set the comments path for the Swagger JSON and UI.
            //   c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "ParkingFeeLicense.xml"));
            //});

            services.AddScoped<ISerial, Serial>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Run(async context =>
                {
                    var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (exceptionHandlerFeature != null)
                    {
                        IdentityModelEventSource.ShowPII = true;
                        var logger = loggerFactory.CreateLogger("Global exception logger");
                        logger.LogError(500,
                                        exceptionHandlerFeature.Error,
                                        $"{exceptionHandlerFeature.Error.Source} {exceptionHandlerFeature.Error.Message} {exceptionHandlerFeature.Error.StackTrace}");
                    }

                    context.Response.StatusCode = 500;
                    await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes($"{exceptionHandlerFeature.Error.Source} {exceptionHandlerFeature.Error.Message}"));
                });
            });

            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint($"{Configuration["ProjectRootUrl"]}/swagger/v{apiVersion}/swagger.json", $"{Configuration["ProjectName"]}");
            //});

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
