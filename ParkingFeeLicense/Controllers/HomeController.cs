﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using NVJUtilities;

namespace ParkingFeeLicense.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration appConfig;

        public HomeController(IConfiguration appConfig)
        {
            this.appConfig = appConfig;
        }

        public IActionResult Index()
        {
            return View();
        }

        //[HttpGet("api/home/get/token")]
        ////[Authorize]
        ////[Authorize(Roles = Role.Admin)]
        //public IActionResult GetToken()
        //{
        //    IdentityModelEventSource.ShowPII = true;
        //    var encryptedKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appConfig["Jwt:Admin:Key"]));
        //    var creds = new SigningCredentials(encryptedKey, SecurityAlgorithms.HmacSha256);
        //    var token = new JwtSecurityToken(
        //        appConfig["Jwt:Issuer"],
        //        appConfig["Jwt:Admin:Audience"],
        //        new[]
        //        {
        //            new Claim(ClaimTypes.Role, Role.Admin),
        //        },
        //        //expires: DateTime.Now.AddDays(1),
        //        signingCredentials: creds
        //      );

        //    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        //}

        [HttpGet("api/home/get/serial/token")]
        [Authorize]
        [Authorize(Roles = Role.Admin)]
        public IActionResult GetSerialToken(string serialNumber, string uniqueId)
        {
            //IdentityModelEventSource.ShowPII = true;
            var encryptedKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appConfig["Jwt:Admin:Key"]));
            var creds = new SigningCredentials(encryptedKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                appConfig["Jwt:Issuer"],
                appConfig["Jwt:Admin:Audience"],
                new[]
                {
                    new Claim("serialNumber", serialNumber),
                    new Claim("uniqueId", uniqueId),
                    new Claim(ClaimTypes.Role, Role.Admin),
                },
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds
              );

            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}