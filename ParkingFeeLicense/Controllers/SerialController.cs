﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using APIModels.Functions;
using APIModels.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NVJUtilities;

namespace ParkingFeeLicense.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SerialController : ControllerBase
    {
        private readonly ISerial serial;

        public SerialController(ISerial serial)
        {
            this.serial = serial;
        }

        [HttpPost]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> Create([FromBody] SerialCreateRequest request)
        {
            var result = await serial.Create(request);
            if (result.StatusCode == HttpStatusCode.BadRequest) return BadRequest(result.Object);
            return Created("", result.Object);
        }

        [HttpPost]
        [Route("activate")]
        public async Task<IActionResult> Activate()
        {

            var uniqueId = User.Claims.First(c => c.Type == "uniqueId").Value;
            var serialNumber = User.Claims.First(c => c.Type == "serialNumber").Value;
            var result = await serial.Activate(uniqueId, serialNumber);
            //if (result.StatusCode == HttpStatusCode.NotFound) return NotFound(result.Object);
            //if (result.StatusCode == HttpStatusCode.Conflict) return Conflict(result.Object);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Validate()
        {
            var uniqueId = User.Claims.First(c => c.Type == "uniqueId").Value;
            var serialNumber = User.Claims.First(c => c.Type == "serialNumber").Value;
            var result = await serial.Validate(uniqueId, serialNumber);
            //if (result?.StatusCode == HttpStatusCode.NotFound) return NotFound(result.Object);
            //if (result?.StatusCode == HttpStatusCode.Conflict) return Conflict(result.Object);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> UnActivate()
        {
            var uniqueId = User.Claims.First(c => c.Type == "uniqueId").Value;
            var serialNumber = User.Claims.First(c => c.Type == "serialNumber").Value;
            var role = ((ClaimsIdentity)User.Identity).Claims
                        .Where(c => c.Type == ClaimTypes.Role)
                        .Select(c => c.Value)
                        .FirstOrDefault();
            var result = await serial.UnActivate(uniqueId, serialNumber, role);
            //if (result.StatusCode == HttpStatusCode.NotFound) return NotFound(result.Object);
            return Ok(result);
        }
    }
}