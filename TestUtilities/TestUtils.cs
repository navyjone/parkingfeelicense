﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestUtilities
{
    public class TestUtils
    {
        public static ParkingFeeLicenseContext GetMemoryContext(string dbName)
        {
            var serviceProvider = new ServiceCollection()
                                    .AddEntityFrameworkInMemoryDatabase()
                                    .BuildServiceProvider();

            var optionsBuilder = new DbContextOptionsBuilder<ParkingFeeLicenseContext>();
            optionsBuilder.UseInMemoryDatabase(dbName)
                            .UseInternalServiceProvider(serviceProvider);
            ParkingFeeLicenseContext context = new ParkingFeeLicenseContext(optionsBuilder.Options);

            return context;
        }
    }
}
