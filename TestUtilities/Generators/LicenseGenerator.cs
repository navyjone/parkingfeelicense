﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class LicenseGenerator
    {
        public static async Task<License> Generate(ParkingFeeLicenseContext context, License license = null)
        {
            if (license is null)
            {
                license = new License();
            }

            license.SerialNumberId = license.SerialNumberId != 0 ? license.SerialNumberId : (await SerialGenerator.Generate(context)).Id;
            license.UniqueId = license.UniqueId ?? Faker.Identification.UKNationalInsuranceNumber();

            await context.Licenses.AddAsync(license);
            await context.SaveChangesAsync();
            context.Entry(license).State = EntityState.Detached;
            return license;
        }
    }
}
