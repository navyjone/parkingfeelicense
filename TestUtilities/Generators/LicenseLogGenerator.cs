﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class LicenseLogGenerator
    {
        public static async Task<LicenseLog> Generate(ParkingFeeLicenseContext context, LicenseLog licenseLog = null)
        {
            if (licenseLog is null)
            {
                licenseLog = new LicenseLog();
            }

            licenseLog.SerialNumberId = licenseLog.SerialNumberId != 0 ? licenseLog.SerialNumberId : (await SerialGenerator.Generate(context)).Id;
            licenseLog.UniqueId = licenseLog.UniqueId ?? Faker.Identification.UKNationalInsuranceNumber();
            licenseLog.ActionBy = licenseLog.ActionBy ?? licenseLog.UniqueId;

            await context.LicenseLogs.AddAsync(licenseLog);
            await context.SaveChangesAsync();
            context.Entry(licenseLog).State = EntityState.Detached;
            return licenseLog;
        }
    }
}
