﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class SerialGenerator
    {
        public static async Task<SerialNumber> Generate(ParkingFeeLicenseContext context, SerialNumber serialNumber = null)
        {
            if (serialNumber is null)
            {
                serialNumber = new SerialNumber();
            }
            
            serialNumber.SiteName = serialNumber.SiteName ?? Faker.Company.Name();
            serialNumber.Number = serialNumber.Number ?? Faker.Identification.UKNationalInsuranceNumber();

            await context.SerialNumbers.AddAsync(serialNumber);
            await context.SaveChangesAsync();
            context.Entry(serialNumber).State = EntityState.Detached;
            return serialNumber;
        }
    }
}
